# Django Tuxfun
- This was my first Django project, I still work on it.
- It is made up of a blog application currently.



Requirements 
------------
- Developed on Python 3.6 
- More dependencies in the requirements.txt file at the root of the project 


Installation
------------

- Clone this repository or download the zip and extact
- Install dependencies 
```
  pip install -r requirements.txt
```
- Create a database ``tuxfun``
- Configure your database settings in ``settings.py``
- Install your database models 
``` 
  python manage.py migrate 
```
- Run the server  
```
  python manage.py runserver 8000 
```
- Navigate to ``http://localhost:8000/``

