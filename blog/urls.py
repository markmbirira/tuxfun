from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from . import views


urlpatterns = [
    # navigation urls
    # ---------------

    url(r'^$', views.blog, name="index"),
    # url(r'^home/$', views.home, name="home"),
    url(r'^trash/$', views.trash, name="trash"),
    url(r'^add/$', views.add, name="add"),

    # blog categories
    # ---------------
    url(r'^(?P<category>[\w]+)$', views.category, name="category"),

    # blog CRUD urls
    # --------------
    url(r'^(?P<post_id>[0-9]+)/$', views.detail, name="detail"),
    url(r'^(?P<post_id>[0-9]+)/trash_detail/$', views.trash_detail, name="trash_detail"),
    url(r'^(?P<post_id>[0-9]+)/edit/$', views.edit, name="edit"),
    url(r'^(?P<post_id>[0-9]+)/delete/$', views.delete, name="delete"),
    url(r'^(?P<post_id>[0-9]+)/delete_trash/$', views.delete_trash, name="delete_trash"),
    url(r'^(?P<post_id>[0-9]+)/comment/$', views.comment, name="comment"),
    url(r'^(?P<post_id>[0-9]+)/like/$', views.like, name="like"),
    url(r'^(?P<post_id>[0-9]+)/share/$', views.share, name="share"),

    # commenting system urls
    # ----------------------
    url(r'^edit_comment/(?P<comment_id>[0-9]+)/$', views.edit_comment, name="edit_comment"),
    url(r'^del_comment/(?P<comment_id>[0-9]+)/$', views.del_comment, name="del_comment"),

    # user management
    # -----------------
    url(r'^sign_up/$', views.sign_up, name="sign_up"),
    url(r'^user_login/$', views.user_login, name="user_login"),
    url(r'^user_logout/$', views.user_logout, name="user_logout"),
    url(r'^users/(?P<user_id>[0-9]+)/$', views.user_profile, name="profile"),
    url(r'^users/edit_profile/(?P<user_id>[0-9]+)/$', views.edit_profile, name="edit_profile"),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
