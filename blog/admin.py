from django.contrib import admin
from .models import Post, Comment, Author

class CommentInline(admin.TabularInline):
    model = Comment
    extra = 2

class PostAdmin(admin.ModelAdmin):
    fieldsets = [
      (None, {"fields": ["post_title","post_text","likes","shares"]}),
      ("Date Information", {"fields": ["pub_date"], "classes": ["collapse"]}),
    ]
    inlines = [CommentInline]
    list_display = ("post_title","post_text","pub_date","likes")
    list_filter = ["pub_date"]
    search_fields = ["post_title"]

class UserAdmin(admin.ModelAdmin):
    fieldsets = [
        ("User information", {"fields": ["username","email","first_name","last_name","is_staff","image"],
                              "classes":["collapse"]}),
    ]
admin.site.register(Post, PostAdmin)
admin.site.register(Author, UserAdmin)
