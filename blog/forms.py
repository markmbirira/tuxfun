from django import forms
from .models import Post, Author
from django.contrib.auth.models import User, AbstractUser
from django.contrib.auth import forms as auth_forms

CATEGORY_CHOICES = (
    ('linux','Linux'),
    ('programming', 'Programming'),
    ('web', 'Web'),
    ('thoughts','Thoughts'),
    ('fun','Fun'),
)

class SampleForm(forms.Form):
    CATEGORY_CHOICES = (
        ('Linux','linux'),
        ('Programming', 'programming'),
        ('Web', 'web'),
    )
    title = forms.CharField(required=True, widget=forms.TextInput(attrs={'class':'form-control'}))
    blog_post = forms.CharField(required=True, widget=forms.Textarea(attrs={'class':'form-control'}))

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['post_title','category','image','post_text']
        widgets = {
            "post_title":forms.TextInput(attrs={'class':'form-control'}),
            "category":forms.Select(choices=CATEGORY_CHOICES, attrs={'class':'form-control'}),
            "post_text":forms.Textarea(attrs={'class':'form-control','cols':'10'})
        }

class EditPostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['post_title','image','post_text']
        widgets = {
            "post_title":forms.TextInput(attrs={'class':'form-control'}),
            "post_text":forms.Textarea(attrs={'class':'form-control'})
        }

class NewUserForm(auth_forms.UserCreationForm):

    class Meta:
        model = Author
        fields = ("username","email")
        # widgets = {
        #     "first_name":forms.TextInput(attrs={'class':'form-control'}),
        #     "last_name":forms.TextInput(attrs={'class':'form-control'}),
        #     "username":forms.TextInput(attrs={'class':'form-control'}),
        #     "email":forms.TextInput(attrs={'class':'form-control', 'type':'email'}),
        #     # "password1":forms.PasswordInput(attrs={'class':'form-control'}),
        #     # "password2":forms.PasswordInput(attrs={'class':'form-control'}),
        # }
    def __init__(self, *args, **kwargs):

        super(NewUserForm, self).__init__(*args, **kwargs)
        # print self.fields
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            # self.fields['username'].widget.attrs['class'] = 'form-control'
            # self.fields['password1'].widget.attrs['class'] = 'form-control'
            # self.fields['password2'].widget.attrs['class'] = 'form-control'

class LoginForm(forms.Form):
    username = forms.CharField(required=True, widget=forms.TextInput(attrs={'class':'form-control'}))
    password = forms.CharField(required=True, widget=forms.TextInput(attrs={'class':'form-control', 'type':'password'}))

class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ['username','first_name','last_name','email','image']
        # exclude = ['password','is_active','is_staff','is_superuser','user_permissions','groups']

    def __init__(self, *args, **kwargs):

        super(EditProfileForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields[field].widget.attrs['required'] = True