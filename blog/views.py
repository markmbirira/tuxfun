from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from .forms import PostForm, EditPostForm, NewUserForm, LoginForm, EditProfileForm
from .models import Post, Comment, Author
from django.core.paginator import Paginator, EmptyPage, InvalidPage, PageNotAnInteger
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


def blog(request):
    posts_list = Post.objects.filter(active=1).order_by('-pub_date')
    paginator = Paginator(posts_list, 5)

    try:
        page = int(request.GET.get("page", 1))

    except:
        page = 1

    try:
        posts = paginator.page(page)

    except(EmptyPage, InvalidPage, PageNotAnInteger):
        posts = paginator.page(paginator.num_pages)

    # breadcrumb feed
    link = reverse('home')
    link1 = reverse('blog:index')

    # block aside feeds
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    context = {'posts': posts, 'recent_5_posts': recent_5_posts,
               'recent_5_fun': recent_5_fun,
               }

    return render(request, 'blog/index.html', context)


def home(request):
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    context = {'recent_5_posts': recent_5_posts,
               'recent_5_fun': recent_5_fun,
               }

    return render(request, 'blog/home.html', context)


@login_required(login_url='/blog/user_login/')
def trash(request):
    posts_list = Post.objects.filter(active=0).order_by('-pub_date')
    paginator = Paginator(posts_list, 5)

    try:
        page = int(request.GET.get("page", 1))

    except:
        page = 1

    try:
        posts = paginator.page(page)

    except(EmptyPage, InvalidPage):
        posts = paginator.page(paginator.num_pages)

    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    context = {'posts': posts, 'recent_5_posts': recent_5_posts,
               'recent_5_fun': recent_5_fun,
               'posts': posts
               }

    return render(request, 'blog/trash.html', context)


def category(request, category):
    posts_list = Post.objects.filter(active=1, category=category).order_by('-pub_date')
    paginator = Paginator(posts_list, 5)

    try:
        page = int(request.GET.get("page", 1))

    except:
        page = 1

    try:
        posts = paginator.page(page)

    except(EmptyPage, InvalidPage):
        posts = paginator.page(paginator.num_pages)

    # breadcrumb feed
    link = reverse('home')
    link1 = reverse('blog:index')

    # block aside feeds
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    context = {'posts': posts, 'recent_5_posts': recent_5_posts,
               'recent_5_fun': recent_5_fun,
               }

    return render(request, 'blog/category.html', context)


def detail(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    comments = post.comment_set.filter(active=1).order_by('-pub_date')
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    context = {'recent_5_posts': recent_5_posts,
               'recent_5_fun': recent_5_fun,
               'post': post,
               'comments': comments,
               }

    return render(request, 'blog/detail.html', context)


@login_required(login_url='/blog/user_login/')
def trash_detail(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    comments = post.comment_set.filter(active=1).order_by('-pub_date')
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    context = {'recent_5_posts': recent_5_posts,
               'recent_5_fun': recent_5_fun,
               'post': post,
               'comments': comments,
               }

    return render(request, 'blog/trash_detail.html', context)


@login_required(login_url='/blog/user_login/')
def add(request):
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)

        if form.is_valid():
            post = form.save(commit=False)
            post.author = get_object_or_404(Author, pk=request.user.id)
            post.pub_date = timezone.now()
            post.mod_date = timezone.now()
            post.save()

            return redirect(reverse('blog:index'))
        else:
            form = PostForm()
            return render(request, 'blog/add.html', {'form': form})

    else:
        form = PostForm()
        context = {'recent_5_posts': recent_5_posts,
                   'recent_5_fun': recent_5_fun,
                   'form': form
                   }
        return render(request, 'blog/add.html', context)


@login_required(login_url='/blog/user_login/')
def edit(request, post_id):
    now = timezone.now()
    post = Post.objects.get(pk=post_id)
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    if request.method == "POST":
        form = EditPostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.pub_date = now
            post.save()

            return redirect(reverse('blog:detail', args=(post.id,)))

    else:
        form = EditPostForm(instance=post)
        context = {'recent_5_posts': recent_5_posts,
                   'recent_5_fun': recent_5_fun,
                   'form': form
                   }
        return render(request, 'blog/edit.html', context)


@login_required(login_url='/blog/user_login/')
def delete(request, post_id):
    post = Post.objects.get(pk=post_id)
    post.active = False
    post.save()
    return redirect(reverse('blog:index'))


@login_required(login_url='/blog/user_login/')
def delete_trash(request, post_id):
    # article is lost for good
    post = get_object_or_404(Post, pk=post_id)
    post.delete()

    return redirect(reverse('blog:index'))


@login_required(login_url='/blog/user_login/')
def comment(request, post_id):
    if request.method == "GET":
        return redirect(reverse('blog:detail', args=(post_id,)))

    elif request.method == "POST":
        now = timezone.now()
        post = get_object_or_404(Post, pk=post_id)
        post.comment_set.create(comment_text=request.POST['comment'], pub_date=now, mod_date=now)

        return redirect(reverse('blog:detail', args=(post.id,)))


@login_required(login_url='/blog/user_login/')
def like(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    post.likes += 1
    post.save()
    return redirect(reverse('blog:detail', args=(post.id,)))


@login_required(login_url='/blog/user_login/')
def share(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    post.pub_date = timezone.now()
    post.shares += 1
    post.save()
    return redirect(reverse('blog:detail', args=(post.id,)))


@login_required(login_url='/blog/user_login/')
def edit_comment(request, comment_id):
    now = timezone.now()
    comment = Comment.objects.get(pk=comment_id)
    post_id = comment.post_id
    if request.method == "GET":
        return render(request, 'blog/edit_comment.html', {'comment': comment})

    else:
        comment.comment_text = request.POST['comment']
        comment.mod_date = now
        comment.save()
        return redirect(reverse('blog:detail', args=(post_id,)))


@login_required(login_url='/blog/user_login/')
def del_comment(request, comment_id):
    comment = get_object_or_404(Comment, pk=comment_id)
    post_id = comment.post_id
    comment.active = False
    comment.save()
    return redirect(reverse('blog:detail', args=(post_id,)))


# user management
# -----------------
def sign_up(request):
    form = NewUserForm()
    if request.method == 'GET':
        return render(request, 'blog/sign_up.html', {'form': form})
    else:
        form = NewUserForm(request.POST)
        if form.is_valid():
            # first_name = form.cleaned_data['first_name']
            # last_name = form.cleaned_data['last_name']
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.clean_password2()

            if password:
                form.save()
                return redirect(reverse('blog:index'))
            else:
                form = NewUserForm()
                return render(request, 'blog/sign_up.html', {'form': form})
        else:
            form = NewUserForm()
            return render(request, 'blog/sign_up.html', {'form': form})


def user_login(request):
    form = LoginForm()
    if request.method == "GET":
        return render(request, 'blog/login.html', {'form': form})
    else:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect(reverse('blog:index'))
        else:
            return redirect(reverse('blog:user_login'))


def user_logout(request):
    logout(request)
    return redirect(reverse('blog:index'))


@login_required(login_url='blog/login')
def user_profile(request, user_id):
    user = get_object_or_404(Author, pk=user_id)
    user_posts = Post.objects.filter(author_id=user_id)
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]

    context = {'user_posts': user_posts, 'recent_5_posts': recent_5_posts,
               'recent_5_fun': recent_5_fun,
               'user': user
               }

    return render(request, 'blog/profile.html', context)


@login_required(login_url='blog/login')
def edit_profile(request, user_id):

    user = get_object_or_404(Author, pk=user_id)
    form = EditProfileForm(instance=user)
    recent_5_posts = Post.objects.filter(active=1).order_by('-pub_date')[:5]
    recent_5_fun = Post.objects.filter(active=1, category='fun').order_by('-pub_date')[:5]
    context = {
        'form':form,
        'recent_5_posts':recent_5_posts,
        'recent_5_fun':recent_5_fun,
    }

    if request.method == "POST":
        form = EditProfileForm(request.POST, instance=user)
        if form.is_valid():
            user = form.save(commit=False)
            user.first_name = form.cleaned_data['first_name']
            user.last_name  = form.cleaned_data['last_name']
            user.image      = form.cleaned_data['image']
            user.save()
            return redirect(reverse('blog:profile', args=(user.id,)))
        # return render(request,'blog/edit_profile.html', context)
        return HttpResponse(user)
    else:
        return render(request,'blog/edit_profile.html', context)
        # return HttpResponse("failed")